Controls:
- WASD: Move camera
- R/F: Go up and down
- Q/E: Tilt camera
- Right Click: Aim camera mode
- Mouse Wheel:
		- While holding right click: Change speed
		- While NOT holding right click: Zoom in and out
- F1: Show wiki
- . / , : Change timewarp

Use the top menubar to show the navigation window and move around planets