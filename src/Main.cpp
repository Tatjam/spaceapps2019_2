#include <sol.hpp>
#include <iostream>
#include "util/Logger.h"
#include "util/Timer.h"
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include "util/InputUtil.h"
#include <imgui/imgui.h>
#include <imgui/imgui_impl_glfw.h>
#include <imgui/imgui_impl_opengl3.h>
#include "system/System.h"

#include "tools/planet_editor/PlanetEditor.h"


InputUtil* input;

/*
void character_callback(GLFWwindow* window, unsigned int codepoint)
{
	ImGuiIO& io = ImGui::GetIO();
	//io.InputQueueCharacters.push_back(codepoint);
	io.AddInputCharacter(codepoint);
}
*/

void do_wiki();

static void glfwError(int id, const char* description)
{
	std::cout << description << std::endl;
}

int main(void)
{
	int WIDTH = 1366;
	int HEIGHT = 780;

	srand((unsigned int)time(NULL));
	rand();

	createGlobalLogger();

	logger->info("Starting SpaceApps");

	glfwSetErrorCallback(&glfwError);
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	GLFWwindow* window = glfwCreateWindow(WIDTH, HEIGHT, "SpaceApps", NULL, NULL);
	glfwMakeContextCurrent(window);
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		logger->fatal("Could not initialize glad");
	}

	createGlobalAssetManager();


	ImGui::CreateContext();
	ImGuiIO& io = ImGui::GetIO();
	ImGui_ImplGlfw_InitForOpenGL(window, true);
	ImGui_ImplOpenGL3_Init("#version 130");
	ImGui::StyleColorsDark();

	// Font for the code editor
	ImFont* font_default = io.Fonts->AddFontDefault();
	ImFont* font_code = io.Fonts->AddFontFromFileTTF("./res/FiraCode-Regular.ttf", 16.0f);

	Timer dtt = Timer();
	float dt = 0.0f;
	float t = 0.0f;

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);
 
	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
  
	bool is_editor = false;
	
	PlanetEditor editor = PlanetEditor(window, "asteroid");

	System system = System(window);
	
	input = new InputUtil();
	input->setup(window); 

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	bool on_star_edit = true;

	int selected = 0;
	float star_factor = 0.5f;

	bool wiki_open = true;
	bool wiki_prev = false;

	while (!glfwWindowShouldClose(window))
	{
		input->update(window);

		glfwGetWindowSize(window, &WIDTH, &HEIGHT);
		glViewport(0, 0, WIDTH, HEIGHT);

		glfwPollEvents();


		ImGui_ImplOpenGL3_NewFrame();
		ImGui_ImplGlfw_NewFrame();
		ImGui::NewFrame();

		if (glfwGetKey(window, GLFW_KEY_F1) == GLFW_PRESS)
		{
			if (!wiki_prev)
			{
				wiki_open = !wiki_open;
			}

			wiki_prev = true;
		}
		else 
		{
			wiki_prev = false;
		}

		if (wiki_open)
		{
			do_wiki();
		}

		if (is_editor)
		{
			
				editor.update(dt, font_code);
		}
		else
		{
			if (on_star_edit)
			{
				ImGui::Begin("Select Star Type", nullptr, ImGuiWindowFlags_AlwaysAutoResize);

				ImGui::Text("F1 will open and close the wiki");

				Star t_star = Star(Star::SO, 1.0f);

				for (size_t i = 0; i < Star::COUNT; i++)
				{
					bool prev_select = selected == i;
					bool select = selected == i;
					std::string type;
					if (i == 0)
					{
						type = "O Type";
					}
					else if (i == 1)
					{
						type = "B Type";
					}
					else if (i == 2)
					{
						type = "A Type";
					}
					else if (i == 3)
					{
						type = "F Type";
					}
					else if (i == 4)
					{
						type = "G Type";
					}
					else if (i == 5)
					{
						type = "K Type";
					}

					else if (i == 6)
					{
						type = "M Type";
					}


					t_star = Star(Star::StarType(i), star_factor);
					ImGui::Checkbox(type.c_str(), &select);
					ImGui::SameLine();
					ImGui::ColorButton("", ImColor(t_star.color.x, t_star.color.y, t_star.color.z));

					if (select && !prev_select)
					{
						selected = (int)i;
					}
				}
				
				t_star = Star(Star::StarType(selected), star_factor);
				ImGui::SliderFloat("Size in class", &star_factor, 0.0f, 1.0f);

				ImGui::Separator();

				ImGui::Text("Mass: %fMo", t_star.mass / 1.98e30);
				ImGui::Text("Radius: %fRo", t_star.radius / 695700000.0);
				ImGui::Text("Temperature: %fK", t_star.temp);

				ImGui::Separator();

				if (ImGui::Button("Create!"))
				{
					system.generate(Star::StarType(selected), star_factor);
					on_star_edit = false;
				}

				ImGui::End();
			}
			else
			{
				system.update(dt * system.time_speed, dt);

				if (system.back_to_star)
				{
					on_star_edit = true;
					system = System(window);
				}
			}
		}

		glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);

		if (is_editor)
		{
			editor.render(WIDTH, HEIGHT);
		}
		else
		{
			if (!on_star_edit)
			{
				system.render(WIDTH, HEIGHT);
			}
		}
		
		ImGui::Render();
		ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
	
		glfwSwapBuffers(window);


		dt = (float)dtt.restart();
		t += dt;
	}

	logger->info("Ending SpaceApps");

	delete input;

	glfwDestroyWindow(window);
	glfwTerminate();

	destroyGlobalAssetManager();
	destroyGlobalLogger();

}