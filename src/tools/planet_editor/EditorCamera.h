#pragma once
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>
#include <GLFW/glfw3.h>
#include "../../util/InputUtil.h"
#include <glm/gtc/matrix_transform.hpp>

class EditorCamera 
{
public:

	glm::dvec3 pos;
	glm::dvec3 up;
	glm::dvec3 forward;

	double zoom;

	bool moved;

	double speed;

	void handle_input(GLFWwindow* window, float dt)
	{
		moved = false;

		// Motion
		if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
		{
			forwards(dt);
			moved = true;
		}

		if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
		{
			backwards(dt);
			moved = true;
		}

		if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
		{
			leftwards(dt);
			moved = true;
		}

		if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
		{
			rightwards(dt);
			moved = true;
		}

		if (glfwGetKey(window, GLFW_KEY_Q) == GLFW_PRESS)
		{
			tilt(dt, -1.0f);
		}

		if (glfwGetKey(window, GLFW_KEY_E) == GLFW_PRESS)
		{
			tilt(dt, 1.0f);
		}

		if (glfwGetKey(window, GLFW_KEY_R) == GLFW_PRESS)
		{
			upwards(dt);
		}

		if (glfwGetKey(window, GLFW_KEY_F) == GLFW_PRESS)
		{
			downwards(dt);
		}

		if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_2))
		{
			glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
			if (input->mouse_delta != glm::dvec2(0.0, 0.0))
			{
				mouse(input->mouse_delta * 0.5, dt);
			}

			if (input->mouse_scroll_delta != 0)
			{
				speed += speed * input->mouse_scroll_delta * 0.05;
			}
		}
		else
		{
			if (input->mouse_scroll_delta != 0)
			{
				zoom += zoom * input->mouse_scroll_delta * 0.05;
			}

			glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
		}
	}

	EditorCamera()
	{
		reset(1.0);
		speed = 1.0;
		zoom = 80.0;
	}

	void reset(double radius)
	{
		pos = glm::dvec3(radius * 2.0, 0.0, 0.0);
		up = glm::dvec3(0.0, 1.0, 0.0);
		forward = glm::dvec3(-1.0, 0.0, 0.0);
		speed = radius / 4.0;
	}

	void forwards(float dt)
	{
		pos += speed * glm::normalize(forward) * (double)dt;
	}

	void backwards(float dt)
	{
		pos -= speed * glm::normalize(forward) * (double)dt;
	}

	void leftwards(float dt)
	{
		pos -= speed * glm::normalize(glm::cross(forward, up)) * (double)dt;
	}

	void rightwards(float dt)
	{
		pos += speed * glm::normalize(glm::cross(forward, up)) * (double)dt;
	}

	void upwards(float dt)
	{
		pos += speed * glm::normalize(up) * (double)dt;
	}

	void downwards(float dt)
	{
		pos -= speed * glm::normalize(up) * (double)dt;
	}


	void tilt(float dt, float dir)
	{
		glm::mat4 f = glm::rotate(glm::dmat4(1.0), (double)dir * (double)dt, forward);
		up = f * glm::dvec4(up, 1.0);
	}

	void mouse(glm::dvec2 deltas, float dt)
	{
		glm::dvec3 or_forward = forward; 
		glm::dvec3 or_up = up;

		// Rotate forward original up
		glm::mat4 hor = glm::rotate(glm::dmat4(1.0), -deltas.x * (double)dt * 0.45, or_up);

		glm::dvec3 right = glm::cross(or_forward, or_up);

		glm::mat4 vert = glm::rotate(glm::dmat4(1.0), -deltas.y * (double)dt * 0.45, right);
		up = vert * glm::dvec4(or_up, 1.0);
		forward = vert * hor * glm::dvec4(forward, 1.0);
		
	}

	glm::dmat4 get_cmodel_matrix()
	{
		return glm::translate(glm::dmat4(1.0f), -pos);
	}

	glm::dmat4 get_view_matrix()
	{
		return glm::lookAt(glm::vec3(0.0f, 0.0f, 0.0f), (glm::vec3)forward, (glm::vec3)up);
	}
};