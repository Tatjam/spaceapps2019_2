#pragma once
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>

class AddCamera
{
public:

	glm::dvec3 pos;
	glm::dvec3 center;
	glm::dvec3 up;
	double c_radius;

	bool moved;

	double zoom;
	double zoomv;

	glm::dmat4 get_cmodel_matrix()
	{
		return glm::translate(glm::dmat4(1.0), -pos);
	}

	glm::dmat4 get_view_matrix()
	{
		// TODO: Better up vector
		return glm::lookAt(glm::dvec3(0.0, 0.0, 0.0), -up, glm::cross(up, glm::dvec3(1.0, 1.0, 1.0)));
	}

	AddCamera()
	{

	}

	void reset(double radius)
	{
		zoom = radius;
		c_radius = radius;
	}

	void update(glm::dvec3 center, glm::dvec3 up, double c_radius)
	{
		this->up = up;
		this->center = center;
		this->c_radius = c_radius;
		pos = center + up * (zoom + c_radius);
	}

	void handle_input(GLFWwindow* window, float dt)
	{
		moved = false;
		
		if (input->mouse_scroll_delta != 0)
		{
			zoomv -= input->mouse_scroll_delta;
		}

		if (zoomv != 0)
		{
			zoom += zoom * zoomv * dt;
			moved = true;

			double prev = zoomv;
			zoomv -= zoomv * dt * 5.0;
			if (zoomv * prev < 0)
			{
				zoomv = 0;
			}
		}
		
	}
};