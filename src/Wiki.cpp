#include <imgui/imgui.h>

void do_wiki()
{
	ImGui::Begin("Wiki", nullptr, ImGuiWindowFlags_AlwaysAutoResize);

	if (ImGui::CollapsingHeader("Stars"))
	{

		if (ImGui::TreeNode("Information"))
		{

			ImGui::BulletText("A star is an astronomical object consisting of a luminous spheroid of plasma held together by its own gravity");
			ImGui::BulletText("In this game we represent only main sequence stars, check types for more info");

			ImGui::TreePop();
		}

		if (ImGui::TreeNode("Types"))
		{

			if (ImGui::TreeNode("M"))
			{
				ImGui::BulletText("Temperature: 2000-3500K");
				ImGui::BulletText("Color: Red");
				ImGui::TreePop();
			}
			if (ImGui::TreeNode("K"))
			{
				ImGui::BulletText("Temperature: 3500-4900K");
				ImGui::BulletText("Color: Orange");
				ImGui::TreePop();
			}
			if (ImGui::TreeNode("G"))
			{
				ImGui::BulletText("Temperature: 4900-6000K");
				ImGui::BulletText("Color: Yellow");
				ImGui::TreePop();
			}
			if (ImGui::TreeNode("F"))
			{
				ImGui::BulletText("Temperature: 6000-7500K");
				ImGui::BulletText("Color: Yellow-White");
				ImGui::TreePop();
			}
			if (ImGui::TreeNode("A"))
			{
				ImGui::BulletText("Temperature: 7500-10000K");
				ImGui::BulletText("Color: White");
				ImGui::TreePop();
			}
			if (ImGui::TreeNode("B"))
			{
				ImGui::BulletText("Temperature: 10000-28000K");
				ImGui::BulletText("Color: Blue-White");
				ImGui::TreePop();
			}
			if (ImGui::TreeNode("O"))
			{
				ImGui::BulletText("Temperature: 28000-50000K.");
				ImGui::BulletText("Color: Blue");
				ImGui::TreePop();
			}

			ImGui::TreePop();
		}

	}

	if (ImGui::CollapsingHeader("Planets"))
	{

		ImGui::BulletText("A planet is an astronomical body orbiting a star or stellar remnant that is massive enough to be rounded by its own gravity, is not massive enough to cause thermonuclear fusion, and has cleared its neighbouring region of any other bodies");
		
		if (ImGui::TreeNode("Types (Represented in the game)"))
		{
			if (ImGui::TreeNode("Ice Planet"))
			{

				ImGui::BulletText("Its surface is fully covered by ice, it's usually far away from its star");
				ImGui::TreePop();

			}
			if (ImGui::TreeNode("Ocean Planet"))
			{

				ImGui::BulletText("An adequate distance to the central star makes this planets be filled with liquid water, in fact, so much water that it's fully covered");
				ImGui::TreePop();

			}
			if (ImGui::TreeNode("Lava Planet"))
			{

				ImGui::BulletText("A planet that is close to the star and has intense geologic processes, leading to a surface covered in lava");
				ImGui::TreePop();

			}
			if (ImGui::TreeNode("Dust Planet"))
			{

				ImGui::BulletText("A small rocky planet with no atmosphere which has been bombarded by asteroid, leading to a cover of dust");
				ImGui::TreePop();

			}
			if (ImGui::TreeNode("Desert Plnaet"))
			{

				ImGui::BulletText("This planet may have previously been a Terra, it does not have liquid water, but used to");
				ImGui::TreePop();

			}
			if (ImGui::TreeNode("Titan-like Planet"))
			{

				ImGui::BulletText("Hydrocarbons fill this planet, including its lakes and rain");
				ImGui::TreePop();

			}
			if (ImGui::TreeNode("Terra Planet"))
			{

				ImGui::BulletText("We live in one, it can have vegetation and animals living in it. It has liquid water and stable geological processes");
				ImGui::TreePop();

			}
			if (ImGui::TreeNode("Gas Planet"))
			{

				ImGui::BulletText("Big planets that orbit far away from their central star, they are usually made of hydrogen with some extra gases, and they have not achieved fusion");
				ImGui::TreePop();

			}
			ImGui::TreePop();
		}

	}
	if (ImGui::CollapsingHeader("Asteroids"))
	{

		ImGui::BulletText("Asteroids are small rocky bodies that don't have enough mass to clear their orbit. They are usually not spherical");

	}

	ImGui::End();
}