#include "Star.h"


/*
double Star::get_radius(double mass0, double age)
{
	double density = 1400;
	double mass = mass0;

	return (glm::pow(0.75 * mass * 1.0 / (density * glm::pi<double>()), 1.0 / 3.0) / 2.2) * 10.0;
}
*/

double Star::get_min_radius(StarType type)
{
	switch (type)
	{
		case SO: return 6.6;
		case SB: return 1.8;
		case SA: return 1.4;
		case SF: return 1.15;
		case SG: return 0.9;
		case SK: return 0.7;
		case SM: return 0.5;
	}

	return 0.0;
}

double Star::get_max_radius(StarType type)
{
	switch (type)
	{
		case SO: return 10.0;
		case SB: return 6.6;
		case SA: return 1.8;
		case SF: return 1.4;
		case SG: return 1.15;
		case SK: return 0.9;
		case SM: return 0.7;
	}

	return 0.0;
}


double Star::get_min_mass(StarType type)
{
	switch (type)
	{
		case SO: return 16.0;
		case SB: return 2.1;
		case SA: return 1.4;
		case SF: return 1.04;
		case SG: return 0.8;
		case SK: return 0.45;
		case SM: return 0.08;
	}

	return 0.0;
}

double Star::get_max_mass(StarType type)
{
	switch (type)
	{
		case SO: return 32.0;
		case SB: return 16.0;
		case SA: return 2.1;
		case SF: return 1.4;
		case SG: return 1.04;
		case SK: return 0.8;
		case SM: return 0.45;
	}

	return 0.0;
}

double Star::get_min_temperature(StarType type)
{
	switch (type)
	{
		case SO: return 30000;
		case SB: return 10000;
		case SA: return 7500;
		case SF: return 6000;
		case SG: return 5200;
		case SK: return 3700;
		case SM: return 2400;
	}

	return 0.0;
}

double Star::get_max_temperature(StarType type)
{
	switch (type)
	{
		case SO: return 50000;
		case SB: return 30000;
		case SA: return 10000;
		case SF: return 7500;
		case SG: return 6000;
		case SK: return 5200;
		case SM: return 3700;
	}

	return 0.0;
}

glm::vec3 Star::get_color(StarType type)
{
	switch (type)
	{
		case SO: return glm::vec3(155.0 / 255.0, 176.0 / 255.0, 255.0 / 255.0);
		case SB: return glm::vec3(170.0 / 255.0, 191.0 / 255.0, 255.0 / 255.0);
		case SA: return glm::vec3(202.0 / 255.0, 215.0 / 255.0, 255.0 / 255.0);
		case SF: return glm::vec3(248.0 / 255.0, 247.0 / 255.0, 255.0 / 255.0);
		case SG: return glm::vec3(255.0 / 255.0, 237.0 / 255.0, 227.0 / 255.0);
		case SK: return glm::vec3(255.0 / 255.0, 218.0 / 255.0, 181.0 / 255.0);
		case SM: return glm::vec3(255.0 / 255.0, 181.0 / 255.0, 108.0 / 255.0);
	}

	return glm::vec3(0, 0, 0);
}

#include "../../util/Logger.h"

Star::Star(StarType type, float radius_factor)
{

	radius = get_min_radius(type) + (radius_factor * (get_max_radius(type) - get_min_radius(type)));
	mass = get_min_mass(type) + (radius_factor * (get_max_mass(type) - get_min_mass(type)));
	temp = get_min_temperature(type) + (radius_factor * (get_min_temperature(type) - get_min_temperature(type)));

	color = get_color(type);
	coef2 = get_coef2(type);

	// 4,591,620,000
	//  695,700,000
	//  695,700,000
	//  149,597,900,000
	radius *= 695700000.0;
	mass *= 1.98e30;
}

float Star::get_coef2(StarType type)
{
	switch (type)
	{
		case SO: return 1.5;
		case SB: return 2.0;
		case SA: return 4.0;
		case SF: return 5.0;
		case SG: return 6.0;
		case SK: return 8.0;
		case SM: return 10.0;
	}

	return 1.0;
}


Star::~Star()
{
}
