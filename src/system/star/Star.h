#pragma once
#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>

class Star
{


public:

	double radius;
	double mass;
	double temp;
	float coef2;
	glm::vec3 color;

	enum StarType
	{
		SO, SB, SA, SF, SG, SK, SM, COUNT
	};


	Star(StarType type, float radius_factor);
	~Star(); 

private:

	// Only for main sequence stars
	double get_min_radius(StarType type);
	double get_max_radius(StarType type);

	double get_min_mass(StarType type);
	double get_max_mass(StarType type);

	double get_min_temperature(StarType type);
	double get_max_temperature(StarType type);

	glm::vec3 get_color(StarType type);
	float get_coef2(StarType type);

};

