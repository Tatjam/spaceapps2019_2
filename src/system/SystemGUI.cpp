#include "System.h"
#include <imgui/imgui.h>

void System::add_common_gui()
{

	if (ImGui::BeginCombo("Parent body", add_center->name.c_str()))
	{
		for (size_t i = 0; i < bodies.size(); i++)
		{
			bool put = false;

			if (bodies[i]->parent == NULL)
			{
				put = true;
			}
			else
			{
				if (bodies[i]->parent->parent == NULL)
				{
					put = true;
				}
			}

			if (bodies[i]->type == Body::BodyType::ASTEROID)
			{
				put = false;
			}

			if (put)
			{
				if (ImGui::Selectable(bodies[i]->name.c_str()))
				{
					add_center = bodies[i];
				}
			}
		}


		ImGui::EndCombo();
	}


	double min_orbit;
	double max_orbit;

	if (add_center == bodies[0])
	{
		min_orbit = add_center->min_orbit;
		max_orbit = add_center->max_orbit;
	}
	else
	{
		min_orbit = add_center->radius * glm::pow(2.0 * add_center->mass / in_mass, 1.0 / 3.0);
		max_orbit = add_center->physics.smajor_axis * glm::pow(add_center->mass / add_center->parent->mass, 2.0 / 5.0);
	}

	ImGui::InputText("Name", in_name, 128);

	ImGui::Text("Orbital Elements: ");

	constexpr double AU_meters = 149597870700.0;



	if (add_center == bodies[0])
	{
		in_elements.smajor_axis /= AU_meters;
		ImGui::InputDouble("Semi-major axis (AU)", &in_elements.smajor_axis);
		in_elements.smajor_axis *= AU_meters;
	}
	else
	{
		in_elements.smajor_axis /= 1e3;
		ImGui::InputDouble("Semi-major axis (km)", &in_elements.smajor_axis);
		in_elements.smajor_axis *= 1e3;
	}

	ImGui::InputDouble("Eccentricity (0->1)", &in_elements.eccentricity);
	ImGui::InputDouble("Inclination (degree)", &in_elements.inclination);
	ImGui::InputDouble("Ascending Node (degree)", &in_elements.asc_node);
	ImGui::InputDouble("Argument of Periapsis (degree)", &in_elements.arg_periapsis);


	in_elements.smajor_axis = glm::clamp(in_elements.smajor_axis, min_orbit, max_orbit);
	in_elements.eccentricity = glm::clamp(in_elements.eccentricity, 0.0, 0.999);
	in_elements.inclination = glm::clamp(in_elements.inclination, 0.0, 360.0);
	in_elements.asc_node = glm::clamp(in_elements.asc_node, 0.0, 360.0);
	in_elements.arg_periapsis = glm::clamp(in_elements.arg_periapsis, 0.0, 360.0);
	in_elements.parent = &add_center->physics;

	double peri = in_elements.smajor_axis * (1.0 - in_elements.eccentricity);
	double apo = in_elements.smajor_axis * (1.0 + in_elements.eccentricity);
	double extra = 8e-12;
	new_taken.r0 = glm::max(peri - glm::pow(in_mass, 0.8) * extra, 0.0);
	new_taken.r1 = apo + glm::pow(in_mass, 0.8) * extra;

}

void generate_gas_colors(Body* target, float methane)
{
	glm::vec3 a0 = glm::vec3(0.62, 0.25, 0.20);
	glm::vec3 a1 = glm::vec3(0.62, 0.42, 0.47);
	glm::vec3 a2 = glm::vec3(0.48, 0.51, 0.62);

	glm::vec3 b0 = glm::vec3(0.78, 0.27, 0.42) * 0.5f;
	glm::vec3 b1 = glm::vec3(0.75, 0.48, 0.47) * 0.6f;
	glm::vec3 b2 = glm::vec3(0.36, 0.51, 0.78) * 0.7f;

	if (methane <= 1.0f)
	{
		target->color_a = glm::mix(a0, a1, methane);
		target->color_b = glm::mix(b0, b1, methane);
	}
	else
	{
		target->color_a = glm::mix(a1, a2, methane - 1.0f);
		target->color_b = glm::mix(b1, b2, methane - 1.0f);
	}
}

void System::add_common_end_gui()
{
	std::string ncopy = std::string(in_name);
	ncopy.erase(std::remove_if(ncopy.begin(), ncopy.end(), ::isspace), ncopy.end());

	bool butresult = false;

	if (ncopy.empty() || (add_type == Body::BodyType::GAS_GIANT && add_center != bodies[0]))
	{
		ImGui::TextDisabled("Create");
	}
	else
	{
		butresult = ImGui::Button("Create");
	}

	if (butresult)
	{
		Body* n_body = new Body();

		if (add_type == ROCKY)
		{
			n_body->type = Body::ROCKY;
		}
		else if (add_type == GAS)
		{
			n_body->type = Body::GAS_GIANT;
			generate_gas_colors(n_body, in_methane);
		}
		else if (add_type == SINGLE_ASTEROID)
		{
			n_body->type = Body::ASTEROID;
		}

		n_body->physics.smajor_axis = in_elements.smajor_axis;
		n_body->physics.eccentricity = in_elements.eccentricity;
		n_body->physics.inclination = in_elements.inclination;
		n_body->physics.asc_node = in_elements.asc_node;
		n_body->physics.arg_periapsis = in_elements.arg_periapsis;

		n_body->name = std::string(in_name);

		n_body->radius = in_radius * 1e3;
		n_body->mass = in_mass;
		n_body->physics.mass = in_mass;
		n_body->set_parent(add_center);
		n_body->update_orbit();

		n_body->seed = rand();

		TakenOrbit torbit;
		double peri = n_body->physics.smajor_axis * (1.0 - n_body->physics.eccentricity);
		double apo = n_body->physics.smajor_axis * (1.0 + n_body->physics.eccentricity);
		double extra = 8e-12;
		torbit.r0 = glm::max(peri - glm::pow(n_body->mass, 0.8) * extra, 0.0);
		torbit.r1 = apo + glm::pow(n_body->mass, 0.8) * extra;
		add_center->taken_orbits.push_back(torbit);

		if (add_type == ROCKY)
		{
			n_body->generate_rocky(in_type);
			n_body->r_type = in_type;
		}

		if (add_type == SINGLE_ASTEROID)
		{
			n_body->generate_rocky(Body::RockyType::ASTEROID_SMALL);
			n_body->r_type = Body::RockyType::ASTEROID_SMALL;
		}

		bodies.push_back(n_body);

		for (size_t i = 0; i < 128; i++)
		{
			in_name[i] = 0;
		}

		add_type = NONE;
	}

	ImGui::SameLine();


	if (ImGui::Button("Cancel"))
	{
		add_type = NONE;
	}
}

char* rocky_type_to_str(Body::RockyType type)
{
	if (type == Body::RockyType::ASTEROID_SMALL)
	{
		return "Small Asteroid";
	}
	else if (type == Body::RockyType::TERRA)
	{
		return "Terra Planet";
	}
	else if (type == Body::RockyType::TITAN)
	{
		return "Titan-like Planet";
	}
	else if (type == Body::RockyType::DUST_DESERT)
	{
		return "Dust Desert Planet";
	}
	else if (type == Body::RockyType::DESERT)
	{
		return "Desert Planet";
	}
	else if (type == Body::RockyType::WATER)
	{
		return "Water Planet";
	}
	else if (type == Body::RockyType::LAVA)
	{
		return "Lava Planet";
	}
	else if (type == Body::RockyType::ICE)
	{
		return "Ice Planet";
	}

	return "?";
}

void System::add_rocky_gui()
{
	ImGui::InputDouble("Radius (km)", &in_radius);

	constexpr double EARTH_RADIUS = 6371;
	in_radius = glm::clamp(in_radius, 0.03 * EARTH_RADIUS, glm::min(20.0 * EARTH_RADIUS, add_center->radius));

	in_density = 5515;
	in_mass = (in_density * (4.0 / 3.0) * glm::pi<double>() * glm::pow(in_radius * 1e3, 3.0));


	ImGui::Text("Mass = %f(mT)", (float)(in_mass / (5.97 * 1e24)));

	sel_name = rocky_type_to_str(in_type);
	if (ImGui::BeginCombo("Planet Type", sel_name))
	{

		for (size_t i = Body::RockyType::TERRA; i < Body::RockyType::COUNT; i++)
		{
			if (ImGui::Selectable(rocky_type_to_str(Body::RockyType(i))))
			{
				in_type = Body::RockyType(i);
			}
		}

		ImGui::EndCombo();
	}



}

void System::add_gas_gui()
{
	ImGui::InputDouble("Radius (km)", &in_radius);

	constexpr double EARTH_RADIUS = 6371;
	in_radius = glm::clamp(in_radius, 6.0 * EARTH_RADIUS, 50.0 * EARTH_RADIUS);

	in_density = 1326;
	in_mass = (in_density * (4.0 / 3.0) * glm::pi<double>() * glm::pow(in_radius * 1e3, 3.0));

	ImGui::SliderFloat("Methane (Outer Layer %)", &in_methane, 0.0f, 2.0f);

	ImGui::Text("Mass = %f(mT)", (float)(in_mass / (5.97 * 1e24)));
}

void System::add_asteroid_gui()
{
	ImGui::InputDouble("Radius (km)", &in_radius);

	in_radius = glm::clamp(in_radius, 10.0, 300.0);

	in_density = 5515;
	in_mass = (in_density * (4.0 / 3.0) * glm::pi<double>() * glm::pow(in_radius * 1e3, 3.0));


	ImGui::Text("Mass = %f(mT)", (float)(in_mass / (5.97 * 1e24)));

}

void System::navigation_gui()
{
	ImGui::Begin("Navigation");

	std::unordered_map<Body*, std::vector<Body*>> parent_moons;

	std::vector<Body*> by_distance;
	for (size_t i = 0; i < bodies.size(); i++)
	{
		if (bodies[i]->parent == bodies[0])
		{
			by_distance.push_back(bodies[i]);
		}
		else if (bodies[i]->parent != nullptr)
		{
			auto it = parent_moons.find(bodies[i]->parent);
			if (it == parent_moons.end())
			{
				parent_moons[bodies[i]->parent] = std::vector<Body*>();

				it = parent_moons.find(bodies[i]->parent);
				it->second.push_back(bodies[i]);
			}
			else
			{
				it->second.push_back(bodies[i]);
			}
		}
	}

	std::sort(by_distance.begin(), by_distance.end(), [](Body* a, Body* b)
	{
		return a->physics.smajor_axis > b->physics.smajor_axis;
	});

	if (ImGui::CollapsingHeader("Planets"))
	{
		for (size_t i = 0; i < by_distance.size(); i++)
		{
			if (by_distance[i]->type == Body::BodyType::ASTEROID)
			{
				continue;
			}

			ImGui::Text(by_distance[i]->name.c_str());
			if (ImGui::IsMouseDoubleClicked(0) && ImGui::IsItemHovered())
			{
				go_to(by_distance[i]);
			}
			// Find moons of this planet
			auto moons = parent_moons[by_distance[i]];
			for (size_t j = 0; j < moons.size(); j++)
			{
				std::string str = " > ";
				str += moons[j]->name;
				ImGui::TextColored(ImVec4(0.7f, 0.7f, 0.7f, 1.0f), str.c_str());

				if (ImGui::IsMouseDoubleClicked(0) && ImGui::IsItemHovered())
				{
					go_to(moons[j]);
				}
			}
		}
	}

	if (ImGui::CollapsingHeader("Asteroids"))
	{
		for (size_t i = 0; i < by_distance.size(); i++)
		{
			if (by_distance[i]->type != Body::BodyType::ASTEROID)
			{
				continue;
			}

			ImGui::Text(by_distance[i]->name.c_str());
			if (ImGui::IsMouseDoubleClicked(0) && ImGui::IsItemHovered())
			{
				go_to(by_distance[i]);
			}
		}
	}


	ImGui::End();
}

void System::add_gui()
{
	ImGuiWindowFlags_ flags = ImGuiWindowFlags_AlwaysAutoResize;

	if (add_type == ROCKY)
	{
		ImGui::Begin("Add Rocky Body", nullptr, flags);
	}
	else if (add_type == GAS)
	{
		ImGui::Begin("Add Gas Body", nullptr, flags);
	}
	else if (add_type == SINGLE_ASTEROID)
	{
		ImGui::Begin("Add Single Asteroid", nullptr, flags);
	}

	if (add_type != NONE)
	{
		add_common_gui();

		ImGui::Separator();
		ImGui::Text("Properties:");

		if (add_type == ROCKY)
		{
			add_rocky_gui();
		}
		else if (add_type == GAS)
		{
			add_gas_gui();
		}
		else if (add_type == SINGLE_ASTEROID)
		{
			add_asteroid_gui();
		}

		add_common_end_gui();
		ImGui::End();
	}

}

void System::menubar()
{


	if (ImGui::BeginMainMenuBar())
	{
		if (ImGui::BeginMenu("File"))
		{
			if (ImGui::MenuItem("New (Not Implemented, reset game)", "", nullptr, false))
			{
				for (size_t i = 0; i < bodies.size(); i++)
				{
					delete bodies[i];
				}
				bodies.clear();

				cam_center = nullptr;

				back_to_star = true;
			}

			if (ImGui::MenuItem("Save (Not Implemented)", "", nullptr, false))
			{

			}

			if (ImGui::MenuItem("Save As (Not Implemented)", "", nullptr, false))
			{

			}

			ImGui::EndMenu();
		}



		if (ImGui::BeginMenu("Add", add_type == NONE))
		{
			if (ImGui::MenuItem("Rocky Body"))
			{
				add_type = ROCKY;
			}

			if (ImGui::MenuItem("Gas Body"))
			{
				add_type = GAS;
			}

			if (ImGui::MenuItem("Single Asteroid"))
			{
				add_type = SINGLE_ASTEROID;
			}

			ImGui::EndMenu();
		}


		if (ImGui::BeginMenu("View"))
		{

			if (ImGui::MenuItem("Navigation"))
			{
				navigation_open = !navigation_open;
			}

			if (ImGui::MenuItem("Reset Camera"))
			{
				if (add_type == NONE)
				{
					camera.reset(star.radius * 2.0);
				}
				else
				{
					add_camera.reset(star.radius * 2.0);
				}
			}

			if (ImGui::MenuItem("Reset Zoom"))
			{
				camera.zoom = 80.0;
			}

			if (ImGui::MenuItem("Wireframe"))
			{
				wireframe = !wireframe;
			}

			if (ImGui::MenuItem("Orbits"))
			{
				show_orbits = !show_orbits;
			}


			ImGui::EndMenu();
		}



		std::string str = "Speed: " + std::to_string(camera.speed) + " m/s = " + std::to_string(camera.speed / 299792458.0) + " c";
		ImGui::MenuItem(str.c_str());

		std::string str2 = "Timewarp: " + std::to_string(time_speed) + "x | Press (, / .) to change";
		ImGui::MenuItem(str2.c_str());

		ImGui::EndMainMenuBar();
	}
}