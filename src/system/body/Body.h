#pragma once
#include "../render/BodyRender.h"
#include <glm/glm.hpp>
#include "NewtonState.h"
#include <vector>
#include <string>
#include "../star/Star.h"
#include "BodyPhysics.h"
#include "../orbit/OrbitDrawer.h"


struct TakenOrbit
{
	double r0, r1;
};

class Body
{
public:
	
	int seed;

	std::vector<TakenOrbit> taken_orbits;

	double radius;
	double min_orbit, max_orbit;

	std::string name;

	enum BodyType
	{
		ROCKY,
		ASTEROID,
		GAS_GIANT
	};

	enum RockyType
	{
		ASTEROID_SMALL,
		TERRA,
		TITAN,
		DUST_DESERT,
		DESERT,
		WATER,
		LAVA,
		ICE,
		COUNT
	};

	RockyType r_type;

	BodyType type;

	// Only used by rocky bodies, gaseous bodies are 
	// drawn by system
	BodyRender render;

	// Gas body render info
	glm::vec3 color_a, color_b;

	NewtonState current;

	double mass;

	// Orbit properties, only defined on bodies with a parent
	BodyPhysics physics;

	OrbitDrawer orbit_drawer;


	Body* parent;

	void generate_rocky(RockyType type);

	void update_orbit();
	void draw_orbit(glm::dmat4 proj_view, glm::dmat4 cmodel, glm::dvec3 camera_pos, float far_plane);

	void set_parent(Body* body);

	Body();
	~Body();
	
private:
	double find_orbit_radius(std::vector<TakenOrbit>& taken, double min, double max);
};

