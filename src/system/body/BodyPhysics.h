#pragma once
#include "NewtonState.h"
#include <glm/gtx/rotate_vector.hpp>
#include <glm/geometric.hpp>
#include <glm/gtx/quaternion.hpp>


#ifndef PI
#define PI 3.14159265358979323846
#endif

#ifndef G
#define G (6.67 * std::pow(10, -11))
#endif
//

struct BodyPhysics
{
	double mass;
	double start_rotation, rotation_speed;
	double eccentricity, smajor_axis, inclination, asc_node, arg_periapsis, true_anomaly;

	BodyPhysics* parent;

	NewtonState to_state_at(double true_anom, double time, bool fast = true) const;
	NewtonState to_state(double time, bool fast = true) const;
	// Gets the state assuming the object has its parent at (0, 0, 0), used by the orbit drawer
	NewtonState to_state_origin(double true_anom) const;

	NewtonState state_from_mean(double mean) const;
	double mean_to_eccentric(double mean, double tol) const;
	double mean_to_true(double mean_anomaly, double tol = 1.0e-14) const;
	double time_to_mean(double time) const;
	double mean_to_time(double mean) const;
	double true_to_eccentric() const;
	double eccentric_to_mean(double eccentric) const;

};