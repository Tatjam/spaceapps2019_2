#include "Body.h"


void Body::generate_rocky(RockyType type)
{
	double atmo_radius = 0.0;

	std::string type_str = "earth";
	if (type == Body::RockyType::ASTEROID_SMALL)
	{
		type_str = "asteroid";
		atmo_radius = 0.0;
	}
	else if (type == Body::RockyType::TERRA)
	{
		type_str = "earth";
		atmo_radius = radius * 1.08;
	}
	else if (type == Body::RockyType::TITAN)
	{
		type_str = "titan";
		atmo_radius = radius * 1.2;
	}
	else if (type == Body::RockyType::DUST_DESERT)
	{
		type_str = "dust_desert";
	}
	else if (type == Body::RockyType::DESERT)
	{
		type_str = "desert";
		atmo_radius = radius * 1.04;
	}
	else if (type == Body::RockyType::WATER)
	{
		type_str = "water";
		atmo_radius = radius * 1.1;
	}
	else if (type == Body::RockyType::LAVA)
	{
		type_str = "lava";
		atmo_radius = radius * 0.0;
	}
	else if (type == Body::RockyType::ICE)
	{
		type_str = "ice";
		atmo_radius = radius * 1.05;
	}
	std::string config_path = "res/planets/" + type_str + "/config.toml";
	std::string script_path = "res/planets/" + type_str + "/surface.lua";

	render.create(radius, atmo_radius, config_path, script_path, seed);
}

void Body::update_orbit()
{
	if (parent != nullptr)
	{
		orbit_drawer.generate(physics);
	}
}

void Body::draw_orbit(glm::dmat4 proj_view, glm::dmat4 cmodel, glm::dvec3 camera_pos, float far_plane)
{
	if (parent != nullptr)
	{
		glm::dvec3 parent_origin = parent->current.pos;
		orbit_drawer.draw(proj_view, cmodel, parent_origin, camera_pos, far_plane, glm::vec3(0.5, 0.5, 0.5));
	}
}

void Body::set_parent(Body* a)
{
	parent = a;
	if (a != nullptr)
	{
		physics.parent = &a->physics;
	}
	else
	{
		physics.parent = nullptr;
	}
}

Body::Body()
{
	set_parent(nullptr);
	render.server = nullptr;
}


Body::~Body()
{
	if (render.server != nullptr)
	{
		delete render.server;
	}
}
