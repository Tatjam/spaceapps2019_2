#pragma once
#include <glm/glm.hpp>


#include "../../planet_mesher/quadtree/QuadTreePlanet.h"
#include "../../planet_mesher/mesher/PlanetTileServer.h"

#include "../../planet_mesher/renderer/PlanetRenderer.h"

#include "../../atmosphere/AtmosphereRenderer.h"


class BodyRender
{
public:
	
	size_t depth;

	QuadTreePlanet planet;
	PlanetTileServer* server;
	PlanetRenderer renderer;
	PlanetMesherInfo mesher_info;
	AtmosphereRenderer atmo_renderer;

	void update(float dt, glm::dvec3 relative_camera, bool moved);
	void draw(glm::dmat4 proj_view, glm::dmat4 cmodel, glm::dvec3 cam_pos, glm::dvec3 origin, float far_plane, float time,
		glm::vec3 lightDir, glm::vec3 atmo_color);
	void create(double radius, double atmo_radius, const std::string& config_path, const std::string& script_path, int seed);
};

