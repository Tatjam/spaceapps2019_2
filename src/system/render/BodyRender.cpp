#include "BodyRender.h"


void BodyRender::update(float dt, glm::dvec3 relative_camera, bool moved)
{
	server->update(planet);
	planet.dirty = false;

	planet.update(*server);

	if (moved)
	{
		glm::vec3 pos_nrm = (glm::vec3)glm::normalize(relative_camera);
		PlanetSide side = planet.get_planet_side(pos_nrm);
		glm::dvec2 offset = planet.get_planet_side_offset(pos_nrm, side);

		double height = std::max(glm::length(relative_camera) - mesher_info.radius - server->get_height(pos_nrm, 8), 1.0);
		height /= mesher_info.radius;
		double depthf = (mesher_info.coef_a - (mesher_info.coef_a * glm::log(height)
			/ ((glm::pow(height, 0.15) * mesher_info.coef_b))) - 0.3 * height) * 0.4;

		depth = (size_t)round(std::max(std::min(depthf, (double)mesher_info.max_depth - 1.0), -1.0) + 1.0);

		planet.set_wanted_subdivide(offset, side, depth);
	}
}

void BodyRender::draw(glm::dmat4 proj_view, glm::dmat4 cmodel, glm::dvec3 cam_pos, glm::dvec3 origin, 
	float far_plane, float time, glm::vec3 lightDir, glm::vec3 atmo_color)
{


	glm::dvec3 cam_pos_rel = (cam_pos - origin);
	glm::dvec3 cam_pos_rel_a = (cam_pos - origin) / mesher_info.atmo_radius;

	glm::dmat4 model = glm::translate(glm::dmat4(1.0), origin);
	model = glm::scale(model, glm::dvec3(mesher_info.radius, mesher_info.radius, mesher_info.radius));
	
	glm::dmat4 amodel = glm::translate(glm::dmat4(1.0), origin);
	amodel = glm::scale(amodel, glm::dvec3(mesher_info.atmo_radius, mesher_info.atmo_radius, mesher_info.atmo_radius));

	float rel_radius = (float)(mesher_info.radius / mesher_info.atmo_radius);

	if (mesher_info.atmo_radius > 0 && glm::length(cam_pos_rel) <= 1.0f)
	{
		atmo_renderer.do_pass(proj_view, cmodel * amodel, far_plane, rel_radius, cam_pos_rel_a, lightDir, atmo_color);
	}

	renderer.render(*server, planet, proj_view, cmodel * model, far_plane, cam_pos_rel, 
		mesher_info.has_water, mesher_info.radius, time, -lightDir, mesher_info.is_water_bright ? 0.8f : 0.0f,
		mesher_info.water_color, mesher_info.deep_color);

	if (mesher_info.atmo_radius > 0 && glm::length(cam_pos_rel) > 1.0f)
	{
		atmo_renderer.do_pass(proj_view, cmodel * amodel, far_plane, rel_radius, cam_pos_rel_a, lightDir, atmo_color);
	}
	
}

void BodyRender::create(double radius, double atmo_radius, const std::string& config_path, const std::string& script_path, int seed)
{
	std::string def_script = assets->loadString(script_path);
	SerializeUtil::read_file_to(config_path, mesher_info, "mesher_info");
	mesher_info.radius = radius;
	mesher_info.atmo_radius = atmo_radius;
	mesher_info.seed = seed;
	mesher_info.max_depth = 12;
	//mesher_info.has_water = false;
	server = new PlanetTileServer(def_script, &mesher_info, seed, FastNoise::Interp::Hermite, true);
}

