#pragma once
#include <vector>
#include "body/Body.h"
#include "../util/Logger.h"
#include "../util/geometry/SphereGeometry.h"
#include "../assets/Shader.h"
#include "../assets/AssetManager.h"
#include "../tools/planet_editor/EditorCamera.h"
#include "../tools/planet_editor/AddCamera.h"
#include <glm/matrix.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "star/Star.h"
#include "body/BodyPhysics.h"
#include <glm/gtx/rotate_vector.hpp>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>



class System
{
private:

	bool time_change;

	Star star;

	// Used for the star
	GLuint vbo, vao, ebo;
	size_t index_count;

	GLuint plane_vbo, plane_vao;

	Shader* shader;
	Shader* gas_shader;
	Shader* atmo_shader;
	Shader* indicator_shader;
	Shader* habitable_shader;

	EditorCamera camera;
	AddCamera add_camera;


	void update_user(float dt);

	// Handles camera locking to planetary
	// bodies and collision
	void update_camera_lock(float dt);

	void on_move();

	bool wireframe;

	bool show_orbits;

	bool navigation_open;

	GLFWwindow* window;

	void render_star(glm::dmat4 proj_view, glm::dmat4 cmodel, float far_plane, glm::dvec3 camera_pos);

	void render_gas(glm::dmat4 proj_view, glm::dmat4 cmodel, glm::dvec3 origin, double radius,
		float far_plane, glm::dvec3 camera_pos, glm::vec3 color_a, glm::vec3 color_b);
	
	void draw_orbit_indicator(glm::dmat4 proj_view, glm::dmat4 cmodel, glm::dmat4 rot, 
		double radius_0, double radius_1, float far_plane, glm::vec3 color);

	void draw_habitable_indicator(glm::dmat4 proj_view, glm::dmat4 cmodel, glm::dmat4 rot,
		double center, double radius, float far_plane);

	float habitable_r, habitable_c;

	enum AddType
	{
		NONE,
		ROCKY,
		GAS,
		SINGLE_ASTEROID
	};


	// Add stuff
	AddType add_type = NONE;
	Body* add_center;

	Body::RockyType in_type;

	TakenOrbit new_taken;

	char* sel_name;
	char in_name[128];
	double in_radius;
	double in_mass;
	double in_density;
	float in_methane;

	BodyPhysics in_elements;

	void add_common_gui();
	void add_common_end_gui();
	void add_rocky_gui();
	void add_gas_gui();
	void add_asteroid_gui();

	void add_gui();
	void navigation_gui();
	void menubar();

	Body* cam_center;
	void go_to(Body* a);



public:

	bool back_to_star;

	float time_speed = 1.0;

	double time;

	std::vector<Body*> bodies;

	void update(double dt, float udt);

	void render(int width, int height);

	void generate(Star::StarType star_type, float radius_factor);

	void imgui_window();

	System(GLFWwindow* window);
	~System();
};

