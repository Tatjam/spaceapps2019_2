#include "System.h"

void System::render_star(glm::dmat4 proj_view, glm::dmat4 cmodel, float far_plane, glm::dvec3 camera_pos)
{
	Body* starb = bodies[0];

	glm::dmat4 bmodel = glm::scale(glm::dmat4(1.0), glm::dvec3(star.radius, star.radius, star.radius));

	glm::dvec3 camera_relative = camera_pos / star.radius;


	shader->use();

	shader->setVec3("camera_relative", camera_relative);
	shader->setMat4("tform", proj_view * cmodel * bmodel);
	shader->setFloat("f_coef", 2.0f / glm::log2(far_plane + 1.0f));
	shader->setVec3("color", star.color);
	shader->setFloat("coef2", star.coef2);


	glBindVertexArray(vao);
	glDrawElements(GL_TRIANGLES, (GLsizei)index_count, GL_UNSIGNED_INT, 0);
	glBindVertexArray(0);

}




void System::render_gas(glm::dmat4 proj_view, glm::dmat4 cmodel, glm::dvec3 origin,
	double radius, float far_plane, glm::dvec3 camera_pos,
	glm::vec3 color_a, glm::vec3 color_b)
{
	glm::dmat4 bmodel = glm::translate(glm::dmat4(1.0), origin);
	bmodel = glm::scale(bmodel, glm::dvec3(radius, radius, radius));

	glm::dvec3 camera_relative = (camera_pos - origin) / (radius * 1.05f);

	glm::dmat4 extra_model = glm::scale(glm::dmat4(1.0), glm::dvec3(1.05, 1.05, 1.05));

	gas_shader->use();

	gas_shader->setVec3("camera_relative", camera_relative);
	gas_shader->setMat4("tform", proj_view * cmodel * bmodel);
	gas_shader->setFloat("f_coef", 2.0f / glm::log2(far_plane + 1.0f));
	gas_shader->setVec3("color_a", color_a);
	gas_shader->setVec3("color_b", color_b);
	gas_shader->setVec3("lightDir", -glm::normalize(origin));
	gas_shader->setFloat("time", (float)time * 0.005f);

	glBindVertexArray(vao);
	glDrawElements(GL_TRIANGLES, (GLsizei)index_count, GL_UNSIGNED_INT, 0);
	glBindVertexArray(0);


	glBlendFunc(GL_SRC_ALPHA, GL_ONE);
	glDepthMask(GL_FALSE);

	atmo_shader->use();
	atmo_shader->setMat4("tform", proj_view * cmodel * bmodel * extra_model);
	atmo_shader->setFloat("f_coef", 2.0f / glm::log2(far_plane + 1.0f));
	atmo_shader->setVec3("camera_pos", camera_relative);
	atmo_shader->setFloat("planet_radius", 1.0f / 1.05f);
	atmo_shader->setVec3("lightDir", -glm::normalize(origin));
	atmo_shader->setVec3("C_R", color_a);

	glBindVertexArray(vao);
	glDrawElements(GL_TRIANGLES, (GLsizei)index_count, GL_UNSIGNED_INT, 0);
	glBindVertexArray(0);

	glDepthMask(GL_TRUE);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

}

void System::draw_orbit_indicator(glm::dmat4 proj_view, glm::dmat4 cmodel, glm::dmat4 rot,
	double radius_0, double radius_1, float far_plane, glm::vec3 color)
{
	glBlendFunc(GL_SRC_ALPHA, GL_ONE);
	glDepthMask(GL_FALSE);

	glm::dmat4 tform = proj_view * cmodel * rot * glm::scale(glm::dmat4(1.0), glm::dvec3(radius_1, radius_1, radius_1));

	indicator_shader->use();
	indicator_shader->setMat4("tform", tform);
	indicator_shader->setFloat("r0", (float)(radius_0 / radius_1));
	indicator_shader->setVec3("color", color);

	glBindVertexArray(plane_vao);
	glDrawArrays(GL_TRIANGLES, 0, 6);
	glBindVertexArray(0);

	glDepthMask(GL_TRUE);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

}

void System::draw_habitable_indicator(glm::dmat4 proj_view, glm::dmat4 cmodel, glm::dmat4 rot,
	double center, double radius, float far_plane)
{
	glBlendFunc(GL_SRC_ALPHA, GL_ONE);
	glDepthMask(GL_FALSE);

	glm::dmat4 tform = proj_view * cmodel * rot * glm::scale(glm::dmat4(1.0), 
		glm::dvec3(center + radius, center + radius, center + radius));

	float r0 = (float)(center / (center + radius));
	float r1 = (float)(radius / (center + radius));

	habitable_shader->use();
	habitable_shader->setMat4("tform", tform);
	habitable_shader->setFloat("r0", r0);
	habitable_shader->setFloat("r1", r1);

	glBindVertexArray(plane_vao);
	glDrawArrays(GL_TRIANGLES, 0, 6);
	glBindVertexArray(0);

	glDepthMask(GL_TRUE);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

void System::render(int width, int height)
{
	if (wireframe)
	{
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	}
	else
	{
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	}

	// ~1 light year
	float far_plane = 1e16f;
	glm::mat4 proj = glm::perspective(glm::radians((float)camera.zoom), (float)width / (float)height, 0.1f, far_plane);

	glm::dmat4 view, cmodel;
	glm::dvec3 camera_pos;

	if (add_type != NONE)
	{
		view = add_camera.get_view_matrix();
		cmodel = add_camera.get_cmodel_matrix();
		camera_pos = add_camera.pos;
	}
	else
	{
		view = camera.get_view_matrix();
		cmodel = camera.get_cmodel_matrix();
		cmodel = cmodel * glm::translate(glm::dmat4(1.0), -cam_center->current.pos);
		camera_pos = camera.pos + cam_center->current.pos;
	}

	glm::dmat4 proj_view = (glm::dmat4)proj * (glm::dmat4)view;

	render_star(proj_view, cmodel, far_plane, camera_pos);
	

	// Render z-sorted
	std::vector<Body*> bodies_sorted = bodies;
	std::sort(bodies_sorted.begin(), bodies_sorted.end(), [camera_pos](Body* a, Body* b)
	{
		return glm::distance2(a->current.pos, camera_pos) > glm::distance2(b->current.pos, camera_pos);
	});

	for (size_t i = 0; i < bodies_sorted.size(); i++)
	{
		Body* body = bodies_sorted[i];
		if (body == bodies[0])
		{
			continue;
		}

		if (body->type == Body::GAS_GIANT)
		{
			render_gas(proj_view, cmodel, body->current.pos, body->radius,
				far_plane, camera_pos, body->color_a, body->color_b);
		}
		else
		{
			glm::vec3 atmo_color = glm::vec3(0.3, 0.7, 1.0);
			if (body->r_type == Body::DESERT)
			{
				atmo_color = glm::vec3(1.0, 0.7, 0.3);
			}
			else if (body->r_type == Body::TITAN)
			{
				atmo_color = glm::vec3(0.3, 0.9, 1.0);
			}
			else if (body->r_type == Body::ICE)
			{
				atmo_color = glm::vec3(0.3, 0.9, 1.0);
			}

			body->render.draw(proj_view, cmodel, camera_pos, body->current.pos, far_plane, (float)time,
				-glm::normalize(body->current.pos), atmo_color);
		}
	}

	if (show_orbits)
	{

		for (size_t i = 1; i < bodies.size(); i++)
		{
			Body* body = bodies[i];
			body->draw_orbit(proj_view, cmodel, camera_pos, far_plane);
		}

		if (add_type != NONE)
		{
			OrbitDrawer drawer = OrbitDrawer();

			glm::dvec3 parent_origin = add_center->current.pos;
			in_elements.parent = &add_center->physics;
			drawer.generate(in_elements);

			drawer.draw(proj_view, cmodel, parent_origin, camera_pos, far_plane, glm::vec3(0.27, 0.67, 0.71));
		}
	}

	if (add_type != NONE)
	{
		// Draw orbit indicators
		glm::dmat4 rot = glm::dmat4(1.0);
		if (add_center != bodies[0])
		{
			rot = glm::rotate(rot, glm::radians(add_center->physics.arg_periapsis), glm::dvec3(0.0, 0.0, 1.0));
			// Inclination 
			rot = glm::rotate(rot, glm::radians(add_center->physics.inclination), glm::dvec3(1.0, 0.0, 0.0));
			// Ascending node
			rot = glm::rotate(rot, glm::radians(add_center->physics.asc_node), glm::dvec3(0.0, 1.0, 0.0));
		}

		for (size_t i = 0; i < add_center->taken_orbits.size(); i++)
		{
			TakenOrbit taken = add_center->taken_orbits[i];

			draw_orbit_indicator(proj_view, cmodel, rot, taken.r0, taken.r1, far_plane, glm::vec3(1.0, 0.0, 0.0));
		}

		draw_orbit_indicator(proj_view, cmodel, rot, new_taken.r0, new_taken.r1, far_plane, glm::vec3(0.0, 0.0, 1.0));

		if (add_center == bodies[0])
		{
			draw_habitable_indicator(proj_view, cmodel, rot, habitable_c, habitable_r, far_plane);
		}
	}

}
