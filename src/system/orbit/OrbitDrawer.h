#pragma once
#include <glad/glad.h>
#include "../body/BodyPhysics.h"
#include <array>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "../../assets/AssetManager.h"
#include "../../assets/Shader.h"

class OrbitDrawer
{
private:

	GLuint vao, vbo;
	Shader* shader;

public:

	static constexpr size_t ITERATIONS = 512;

	void generate(BodyPhysics elems);
	void draw(glm::dmat4 proj_view, glm::dmat4 cmodel, glm::dvec3 origin, glm::dvec3 camera_pos, float far_plane, glm::vec3 color);

	OrbitDrawer();
	~OrbitDrawer();
};

