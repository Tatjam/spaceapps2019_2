#include "OrbitDrawer.h"



void OrbitDrawer::generate(BodyPhysics elems)
{

	if (vao != 0)
	{
		glDeleteVertexArrays(1, &vao);
		glDeleteBuffers(1, &vbo);
		vao = 0; 
		vbo = 0;
	}

	glGenVertexArrays(1, &vao);
	glGenBuffers(1, &vbo);



	std::array<glm::vec3, ITERATIONS * 2 + 2> points;

	double true_anom = elems.mean_to_true(0.0);
	NewtonState st = elems.to_state_origin(true_anom);

	glm::vec3 prev;
	prev = st.pos;

	for (size_t i = 0; i < ITERATIONS; i++)
	{
		double theta = ((double)i / (double)(ITERATIONS - 1)) * glm::two_pi<double>();
		double true_anom = elems.mean_to_true(theta);
		NewtonState st = elems.to_state_origin(true_anom);

		points[i * 2 + 0] = prev;
		points[i * 2 + 1] = st.pos;

		prev = st.pos;
	}

	glBindVertexArray(vao);

	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * points.size(), points.data(), GL_STATIC_DRAW);

	// pos
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void OrbitDrawer::draw(glm::dmat4 proj_view, glm::dmat4 cmodel, glm::dvec3 origin, glm::dvec3 camera_pos, float far_plane, glm::vec3 color)
{
	shader->use();

	glm::dmat4 model = glm::translate(glm::dmat4(1.0), origin);

	shader->setMat4("tform", proj_view * cmodel * model);
	shader->setFloat("f_coef", 2.0f / glm::log2(far_plane + 1.0f));
	shader->setVec3("color", color);
	shader->setVec3("camera_pos", camera_pos);
	shader->setVec3("origin", origin);

	glBindVertexArray(vao);
	glDrawArrays(GL_LINES, 0, (GLsizei)(ITERATIONS * 2 + 2));
	glBindVertexArray(0);
}

OrbitDrawer::OrbitDrawer()
{
	vao = 0;
	vbo = 0;
	
	shader = assets->get<Shader>("system/orbit");
}


OrbitDrawer::~OrbitDrawer()
{
	if (vao != 0)
	{
		glDeleteVertexArrays(1, &vao);
		glDeleteBuffers(1, &vbo);
		vao = 0;
		vbo = 0;
	}
}
