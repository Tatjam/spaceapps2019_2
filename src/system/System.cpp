#include "System.h"
#include <imgui/imgui.h>
#include "../util/InputUtil.h"

void System::on_move()
{

}



void System::update_user(float dt)
{
	update_camera_lock(dt);

	if (add_type != NONE)
	{
		if (add_center == nullptr)
		{
			add_center = bodies[0];
		}

		glm::dvec3 up = glm::dvec3(0.0, 1.0, 0.0);

		if (add_center != bodies[0])
		{
			up = glm::rotateY(up, glm::radians(add_center->physics.arg_periapsis));
			// Inclination 
			up = glm::rotateX(up, glm::radians(add_center->physics.inclination + 180.0));
			// Ascending node
			up = glm::rotateY(up, glm::radians(add_center->physics.asc_node));
		}

		add_camera.update(add_center->current.pos, up, add_center->radius);
	}


	if (!ImGui::IsAnyItemFocused() && !ImGui::IsAnyItemActive() && !ImGui::IsAnyWindowFocused())
	{
		bool moved = false;

		if (add_type != NONE)
		{
			add_camera.handle_input(window, dt);

			if (add_camera.moved)
			{
				on_move();
			}
		}
		else
		{
			camera.handle_input(window, dt);

			if (camera.moved)
			{
				on_move();
			}
		}
	
		bool any = false;

		if (glfwGetKey(window, GLFW_KEY_PERIOD) == GLFW_PRESS)
		{
			if (!time_change)
			{
				time_speed += time_speed;
				if (time_speed >= 262144)
				{
					time_speed = 262144;
				}

				time_change = true;
			}

			any = true;
		}

		if (glfwGetKey(window, GLFW_KEY_COMMA) == GLFW_PRESS)
		{
			if (!time_change)
			{
				time_speed -= time_speed / 2.0f;
				if (time_speed <= 0)
				{
					time_speed = 0;
				}

				time_change = true;
			}

			any = true;
		}

		time_change = any;


	}
	else
	{
		glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
	}
}

void System::go_to(Body* a)
{
	glm::dvec3 a_pos = camera.pos;
	camera.reset(a->radius);
	glm::dvec3 ending = glm::normalize(a->current.pos - a_pos);
	cam_center = a;

	camera.pos = -ending * a->radius * 2.0;
	camera.forward = ending;
	camera.up = glm::cross(ending, glm::dvec3(1.0, 0.0, 0.0));
}

void System::update(double dt, float udt)
{
	menubar();
	update_user(udt);

	if (navigation_open)
	{
		navigation_gui();
	}

	add_gui();


	time += dt;

	for (size_t i = 1; i < bodies.size(); i++)
	{
		Body* body = bodies[i];
		body->physics.true_anomaly = body->physics.mean_to_true(body->physics.time_to_mean(time));
	}

	bodies[0]->current = NewtonState();
	bodies[0]->current.pos = glm::dvec3(0, 0, 0);

	for (size_t i = 1; i < bodies.size(); i++)
	{
		Body* body = bodies[i];
		NewtonState n_state = body->physics.to_state(time);
		body->current = n_state;
	}

	for (size_t i = 1; i < bodies.size(); i++)
	{
		Body* body = bodies[i];

		if (body->type != Body::GAS_GIANT)
		{
			body->render.update((float)dt, camera.pos, camera.moved);
		}
	}
}

void System::generate(Star::StarType star_type, float radius_factor)
{
	star = Star(star_type, radius_factor);
	Body* starb = new Body();
	starb->name = "New Sun";
	starb->mass = star.mass;
	starb->radius = star.radius;
	starb->physics.mass = star.mass;
	starb->min_orbit = star.radius * 20.0;
	starb->max_orbit = star.radius * 15000.0;
	
	constexpr double AU_meters = 149597870700;
	double sun_to_radius = star.radius / AU_meters;
	double fac = 1.0 / 215.0;


	bodies.push_back(starb);

	camera.reset(star.radius);
	add_camera.reset(star.radius);

	add_center = bodies[0];
	cam_center = bodies[0];

	double sun_rel = star.temp / 5200.0;
	double sun_hab = 1.45 * AU_meters;
	double sun_rad = 0.7 * AU_meters;

	habitable_c = (float)(sun_rel * sun_hab);
	habitable_r = (float)(sun_rel * sun_rad);

}


void System::update_camera_lock(float dt)
{
	/*if (cam_center != bodies[0])
	{
		double dist = glm::distance(cam_center->current.pos, camera.pos);
		if (dist >= cam_center->max_orbit * 1.5)
		{
			cam_center = bodies[0];
		}

		// Check collision
	}
	*/

	Body* canditate = bodies[0];
	double candidate_dist = 9e100;

	// Try to lock to nearest sphere of influence
	for (size_t i = 1; i < bodies.size(); i++)
	{
		double dist = glm::distance(bodies[i]->current.pos, camera.pos + cam_center->current.pos);

		if (dist <= bodies[i]->radius * 50.0 && dist < candidate_dist)
		{
			canditate = bodies[i];
			candidate_dist = dist;
		}
	}

	camera.pos += cam_center->current.pos;
	cam_center = canditate;
	camera.pos -= cam_center->current.pos;
}

void System::imgui_window()
{
	
}

System::System(GLFWwindow* window) : star(Star::SO, 0.0f)
{
	in_density = 0.0;
	in_elements = BodyPhysics();
	for (size_t i = 0; i < 128; i++)
	{
		in_name[i] = 0;
	}
	in_mass = 0.0;
	in_radius = 0.0;
	wireframe = false;

	add_type = NONE;
	this->window = window;

	time = 0;

	in_type = Body::TERRA;

	SphereGeometry::generate_and_upload(&vao, &vbo, &ebo, &index_count, 32);
	shader = assets->get<Shader>("system/star");
	gas_shader = assets->get<Shader>("system/gas");
	atmo_shader = assets->get<Shader>("atmosphere/atmo");
	indicator_shader = assets->get<Shader>("system/orbit_indicator");
	habitable_shader = assets->get<Shader>("system/green_indicator");

	add_center = nullptr;
	navigation_open = false;

	show_orbits = true;

	// Generate the plane
	float vertices[3 * 6] =
	{
		// x      y       z
		-1.0f,	0.0f,	-1.0f,
		-1.0f,	0.0f,	1.0f,
		1.0f,	0.0f,	-1.0f,
		1.0f,	0.0f,	-1.0f,
		-1.0f,	0.0f,	1.0f,
		1.0f,	0.0f,	1.0f,
	};

	glGenVertexArrays(1, &plane_vao);
	glGenBuffers(1, &plane_vbo);

	glBindVertexArray(plane_vao);

	glBindBuffer(GL_ARRAY_BUFFER, plane_vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	// pos
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	back_to_star = false;



}


System::~System()
{
}
