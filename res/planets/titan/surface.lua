
-- Returns named items modificable by the user here
-- Types can be:
-- num, vec2, vec3, color
function get_data_items()
	return {}
end

-- Data set by caller:
--
--	coord_3d	-> {x, y, z}	-> Spherical 3D coordinates
--	coord_2d	-> {x, y}		-> Equirrectangular 2D coordinates (azimuth, elevation)
--	depth		-> int			-> Tile depth
--	radius		-> float		-> Radius of the planet
--	data		-> table		-> Data set by the user
--
-- Also, all utility terrain generating functions can be used
-- You should return height at said point in meters, can be negative

local iron = make_color(0.91,0.35,0.21)
local methane = make_color(0.71,0.65,0.76)
local sand = make_color(1.0,0.56,0.5)

function biome(x, y, z)

	noise.set_frequency(3.0);
	noise.set_fractal_billow();
	noise.set_fractal_octaves(10);
	noise.set_fractal_gain(0.5);

	local n = noise.perlin3_fractal(x, y, z);

	return (clamp(n * 3.0 + 2.0, -2.0, 2.0) + 2.0) * 0.5;

end

function lake(x, y, z)

	noise.set_frequency(0.6);
	noise.set_fractal_fbm();
	noise.set_fractal_octaves(5);
	noise.set_fractal_gain(0.6);

	return math.min(noise.perlin3_fractal(x, y, z) * 5.3, 0.0);

end

function generate()

	local biome = biome(coord_3d.x, coord_3d.y, coord_3d.z);
	local lakes = lake(coord_3d.x, coord_3d.y, coord_3d.z);
	local col;

	if biome < 1.0 then

		col = mix_color(iron, sand, biome);

	else
		col = mix_color(sand, methane, biome - 1.0);
	end

	height = (biome + lakes) * 0.5 * radius * 0.01;
	color = col;
end

