
-- Returns named items modificable by the user here
-- Types can be:
-- num, vec2, vec3, color
function get_data_items()
	return {}
end

-- Data set by caller:
--
--	coord_3d	-> {x, y, z}	-> Spherical 3D coordinates
--	coord_2d	-> {x, y}		-> Equirrectangular 2D coordinates (azimuth, elevation)
--	depth		-> int			-> Tile depth
--	radius		-> float		-> Radius of the planet
--	data		-> table		-> Data set by the user
--
-- Also, all utility terrain generating functions can be used
-- You should return height at said point in meters, can be negative

function surface(x, y, z)

	noise.set_fractal_fbm();
	noise.set_frequency(9.0);
	noise.set_fractal_octaves(14);
	noise.set_fractal_gain(0.5);

	return math.abs(noise.perlin3_fractal(x, y, z)) - 0.05;


end

local red = make_color(1.0, 0.08, 0.10);
local black = make_color(0.15, 0.07, 0.10);

function generate()

	local s = surface(coord_3d.x, coord_3d.y, coord_3d.z);
	height = s * radius * 0.03;
	color = mix_color(red, black, clamp(math.abs(s * 20.0), 0.0, 1.0));
end

