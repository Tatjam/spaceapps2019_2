
-- Returns named items modificable by the user here
-- Types can be:
-- num, vec2, vec3, color
function get_data_items()
	return {}
end

-- Data set by caller:
--
--	coord_3d	-> {x, y, z}	-> Spherical 3D coordinates
--	coord_2d	-> {x, y}		-> Equirrectangular 2D coordinates (azimuth, elevation)
--	depth		-> int			-> Tile depth
--	radius		-> float		-> Radius of the planet
--	data		-> table		-> Data set by the user
--
-- Also, all utility terrain generating functions can be used
-- You should return height at said point in meters, can be negative

local highlands = make_color(0.9, 0.9, 1.0);
local lowlands = make_color(0.8, 0.8, 0.9);

function geometry(x, y, z)

	noise.set_fractal_octaves(10);
	noise.set_fractal_rigidmulti();
	noise.set_frequency(5.0);
	noise.set_fractal_octaves(10);
	noise.set_fractal_gain(0.7);

	local c = noise.simplex3_fractal(x, y, z);

	local dfreq = 1.7;

	local d = noise.perlin3_fractal(x * dfreq, y * dfreq, z * dfreq);

	return c * 0.9 - d * 0.5;

end


function sub(x, y, z)

	noise.set_frequency(10000.0);

	return noise.perlin3(x, y, z) + 1.0;

end

function generate()

	local g = geometry(coord_3d.x, coord_3d.y, coord_3d.z);
	local sub = sub(coord_3d.x, coord_3d.y, coord_3d.z) * 0.3;

	height = (g * radius * 0.05) * 0.01 + 10000.0;
	color = mix_color(lowlands, highlands, clamp(g + sub, 0.0, 1.0));
end

