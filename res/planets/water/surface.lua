
-- Returns named items modificable by the user here
-- Types can be:
-- num, vec2, vec3, color
function get_data_items()
	return {}
end

-- Data set by caller:
--
--	coord_3d	-> {x, y, z}	-> Spherical 3D coordinates
--	coord_2d	-> {x, y}		-> Equirrectangular 2D coordinates (azimuth, elevation)
--	depth		-> int			-> Tile depth
--	radius		-> float		-> Radius of the planet
--	data		-> table		-> Data set by the user
--
-- Also, all utility terrain generating functions can be used
-- You should return height at said point in meters, can be negative

function islands(x, y, z)

	noise.set_frequency(100.0);

	noise.set_fractal_fbm();
	noise.set_fractal_octaves(5);

	local i = noise.perlin3_fractal(x, y, z);

	return i - 0.7;

end

local sand = make_color(0.79,0.65,0.40);
local high = make_color(0.9, 0.7, 0.6);

function generate()

	local i = islands(coord_3d.x, coord_3d.y, coord_3d.z);
	local i2 = i;
	height = i * radius * 0.001;
	color = mix_color(high, sand, clamp(i, 0.0, 0.5));
end

