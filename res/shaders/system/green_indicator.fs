#version 330 core

in vec3 vPos;
in vec3 vNormal;
in float flogz;

uniform float f_coef;

out vec4 FragColor;

uniform float r0; //center
uniform float r1; //radius
uniform vec3 color;

void main()
{
   

    float dist = abs(length(vPos) - r0);

    float alpha = 0.0;

    if(dist < r1)
    {
        alpha = max(1.0 - (dist / r1), 0.2);
    }

    FragColor = vec4(vec3(0.0, 1.0, 0.0), alpha);

    // Could be removed for that sweet optimization, but some
    // clipping can happen on weird planets
    gl_FragDepth = log2(flogz) * f_coef * 0.5;
}