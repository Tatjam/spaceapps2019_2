#version 330 core

in vec3 vPos;
in vec3 vNormal;
in float flogz;

uniform float f_coef;

uniform float time;

out vec4 FragColor;

uniform float coef2;

uniform vec3 color_a;
uniform vec3 color_b;
uniform vec3 lightDir;

uniform vec3 camera_relative;

// Simplex 2D noise
//
vec3 permute(vec3 x) { return mod(((x*34.0)+1.0)*x, 289.0); }

float snoise(vec2 v){
  const vec4 C = vec4(0.211324865405187, 0.366025403784439,
           -0.577350269189626, 0.024390243902439);
  vec2 i  = floor(v + dot(v, C.yy) );
  vec2 x0 = v -   i + dot(i, C.xx);
  vec2 i1;
  i1 = (x0.x > x0.y) ? vec2(1.0, 0.0) : vec2(0.0, 1.0);
  vec4 x12 = x0.xyxy + C.xxzz;
  x12.xy -= i1;
  i = mod(i, 289.0);
  vec3 p = permute( permute( i.y + vec3(0.0, i1.y, 1.0 ))
  + i.x + vec3(0.0, i1.x, 1.0 ));
  vec3 m = max(0.5 - vec3(dot(x0,x0), dot(x12.xy,x12.xy),
    dot(x12.zw,x12.zw)), 0.0);
  m = m*m ;
  m = m*m ;
  vec3 x = 2.0 * fract(p * C.www) - 1.0;
  vec3 h = abs(x) - 0.5;
  vec3 ox = floor(x + 0.5);
  vec3 a0 = x - ox;
  m *= 1.79284291400159 - 0.85373472095314 * ( a0*a0 + h*h );
  vec3 g;
  g.x  = a0.x  * x0.x  + h.x  * x0.y;
  g.yz = a0.yz * x12.xz + h.yz * x12.yw;
  return 130.0 * dot(m, g);
}

void main()
{
    float freq = 3.0;
    float freq2 = 5.5 + sin(time * 0.04) * 0.2;
    float freq3 = 20.0;

    float diffuse = dot(lightDir, vNormal);
    
    float hfdrive = snoise(vec2(vPos.y * 4.0, vPos.x * 4.0));
    
    float hfreq = snoise(vec2(atan((vPos.z) / (vPos.x)) * freq3 * 0.3 + time * 0.09 + hfdrive * 0.3, acos(vPos.y) * freq3 + time * 0.3 + hfdrive * 0.3));

    float sphere_drive = snoise(vec2(atan((vPos.z) / (vPos.x)) * freq2 + time * 0.3, acos(vPos.y) * freq2 + time * 0.3)) + hfreq * 0.3;


    float stripe = clamp(snoise(vec2(vPos.y * 4.0 + sphere_drive * 0.09, 0.0)), 0.0, 1.0);


    vec2 spherical = vec2(atan((vPos.z) / (vPos.x)) * freq + sphere_drive * 0.05, acos(vPos.y) * freq + sphere_drive * 0.05);

    stripe += snoise(spherical) * 0.3;
    stripe = max(min(stripe, 1.0), -1.0);

    vec3 col = (color_a * stripe + color_b * (1.0 - stripe)) + hfreq * 0.01;

    FragColor = vec4(col * diffuse, 1.0);

    // Could be removed for that sweet optimization, but some
    // clipping can happen on weird planets
    gl_FragDepth = log2(flogz) * f_coef * 0.5;
}