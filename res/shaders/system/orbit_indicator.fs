#version 330 core

in vec3 vPos;
in vec3 vNormal;
in float flogz;

uniform float f_coef;

out vec4 FragColor;

uniform float r0;
uniform vec3 color;

void main()
{
   

    float dist = length(vPos);

    float alpha = 0.0;

    if(dist > r0 && dist < 1.0)
    {
        alpha = 0.5;
    }

    FragColor = vec4(color, alpha);

    // Could be removed for that sweet optimization, but some
    // clipping can happen on weird planets
    gl_FragDepth = log2(flogz) * f_coef * 0.5;
}