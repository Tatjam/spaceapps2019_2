#version 330 core

in vec3 vPos;
in vec3 vNormal;
in float flogz;

uniform float f_coef;

out vec4 FragColor;

uniform float coef2;

uniform vec3 color;

uniform vec3 camera_relative;

vec3 rim(vec3 color, vec3 icolor, float start, float end, float coef, float coef_b)
{
  vec3 normal = normalize(vNormal);
  vec3 eye = normalize(camera_relative);
  float rim = smoothstep(start, end, 1.0 - dot(normal, eye));
  float a = pow(clamp(rim, 0.0, 1.0), coef) * coef_b;
  return a * color + icolor * (1.0 - a);
}

void main()
{
    vec3 lDir = normalize(vec3(1.0, 0.0, 0.0));

    vec3 saturated = color * 1.7;
    saturated = vec3(min(saturated.x, 1.0), min(saturated.y, 1.0), min(saturated.z, 1.0));

    vec3 col = rim(color, saturated, 0.0, 1.0, 0.65, coef2);


    FragColor = vec4(col, 1.0);

    // Could be removed for that sweet optimization, but some
    // clipping can happen on weird planets
    gl_FragDepth = log2(flogz) * f_coef * 0.5;
}