#version 330 core

out vec4 FragColor;
  
in vec3 vColor;
in vec2 vTexture;
in vec3 vNormal;
in vec3 vPos;

uniform vec3 lightDir;

in float flogz;

uniform float f_coef;


void main()
{

    float diff = max(dot(-lightDir, vNormal), 0.0);

    vec3 col = vColor;

    FragColor = vec4(diff * col, 1.0);

    // Could be removed for that sweet optimization, but some
    // clipping can happen on weird planets
    gl_FragDepth = log2(flogz) * f_coef * 0.5;
}